# Food Service
Service ini adalah food service dimana user bisa menambahkan review makanan yang dimana list makanan sudah di inputkan oleh admin.
## Specification
- Web : Flask
- ORM : Sqlalchemy
- JWT : flask_jwt_extended
- Database : MySQL
## Architecture
main->init->router->controller->model->Database

# REST API
## Get Token
- `GET` `AllUser` {host}/user/requesttoken
  ```json
    {
        "name" : "admin1",
        "email": "admin@admin.com"
    }
  ```

## Users
- `GET` `JWT` `Admin` {host}/users
- `GET` `JWT` `AllUser` {host}/user
- `POST` `JWT` `AllUser` {host}/user/insert
  ```json
    {
        "name" : "admin1",
        "email": "admin@admin.com",
        "is_admin" : 1
    }
  ```
- `POST` `JWT` `AllUser` {host}/user/update
  ```json
    {
        "name" : "admin1",
        "email": "admin@admin.com"
    }
  ```
- `DELETE` `JWT` `Admin` {host}/user/delete/:iduser

## Foods
- `GET` `AllUser` {host}/foods
- `GET` `AllUser` {host}/food/:idfood
- `POST` `JWT` `Admin` {host}/food/insert
  ```json
    {
      "name" : "seblak",
      "detail": "hot jeletot seblak"
    }
  ```
- `POST` `JWT` `Admin` {host}/food/update/:idfood
  ```json
    {
      "name" : "seblak",
      "detail": "hot jeletot seblak"
    }
  ```
- `DELETE` `JWT` `Admin` {host}/food/delete/:idfood

## Reviews
- `GET` `JWT` `AllUser` {host}/reviews
- `GET` `JWT` `AllUser` {host}/review/:idreview
- `POST` `JWT` `AllUser` {host}/review/insert
  ```json
    {
      "food_id" : 3,
      "rating": 5,
      "review": "pisangnya pulen"
    }
  ```
- `POST` `JWT` `AllUser` {host}/review/update/:idreview
  ```json
    {
      "food_id" : 3,
      "rating": 5,
      "review": "pisangnya pulen"
    }
  ```
- `DELETE` `JWT` `AllUser` {host}/review/delete/:idreview

# FLOW
## Admin
- Pertama daftar dahulu dengan insert user dengan request is_admin = 1
- Setelah itu request token JWT nya
- Lalu Admin dapat melakukan semua hal yang ada di fungsi aplikasi ini, terutama melihat semua user, menambahkan edit dan hapus makanan
## User
- Pertama daftar dahulu dengan insert user dengan request is_admin = 0
- Setelah itu request token JWT nya
- Lalu User bisa mengubah email dan nama pada user, melihat list dan detail makanan, dan dapat melihat menambahkan update dan delete review review

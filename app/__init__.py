from flask import Flask
from config import Config
from flask_jwt_extended import JWTManager


app = Flask(__name__)
app.config.from_object(Config)
jwt = JWTManager(app)

from app.routers.user_router import *
from app.routers.food_router import *
from app.routers.review_router import *

app.register_blueprint(users_blueprint)
app.register_blueprint(foods_blueprint)
app.register_blueprint(reviews_blueprint)


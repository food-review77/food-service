from os import access
from app.models.food_model import Foods as mysqldb
from app.models.user_model import Users as mysqldb_user
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime


def showAll():
    data = mysqldb.showFoods()
    result = []
    for items in data:
        food = {
            "id" : items.id,
            "name" : items.name,
            "detail" : items.detail,
            "created_at" : items.created_at,
            "updated_at" : items.updated_at
        }
        
        result.append(food)
    
    return jsonify({"message":"Success","data":result})
       
def show(id):
    data = mysqldb.showFoodById(id)
    
    if data.count() == 0:
        return jsonify({"message":"data not found"})

    food = {
        "id" : data[0].id,
        "name" : data[0].name,
        "detail" : data[0].detail,
        "created_at" : data[0].created_at,
        "updated_at" : data[0].updated_at
    }

    return jsonify({"message":"Success","data":food})

@jwt_required()
def insert(params):
    token = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(token)
    
    if dbresult.count() != 0:
        if dbresult[0].is_admin == 1:
            params['created_by'] = dbresult[0].id
            data = mysqldb.insertFood(params)
            return jsonify({"message" : "Success"})
        else:
            return jsonify({"message" : "Unauthorized"})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def update(id,params):
    token = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(token)

    if dbresult.count() != 0:
        if dbresult[0].is_admin == 1:
            data = mysqldb.updateFoodById(id, params)
            return jsonify({"message" : "Success"})
        else:
            return jsonify({"message" : "Unauthorized"})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def delete(id):
    params = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(params)

    if dbresult.count() != 0:
        if dbresult[0].is_admin == 1:
            data = mysqldb.deleteFoodById(id)
            return jsonify({"message" : "Success"})
        else:
            return jsonify({"message" : "Unauthorized"})
    else:
        return jsonify({"message" : "Token Failed"})


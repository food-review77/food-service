from os import access
from app.models.review_model import Reviews as mysqldb
from app.models.user_model import Users as mysqldb_user
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

@jwt_required()
def showAll():
    params = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(params)

    if dbresult.count() != 0:
        data = mysqldb.showReviews(dbresult[0].id)
        
        if data.count() == 0:
            return jsonify({"message":"data not found"})

        result = []
        for items in data:
            review = {
                "id" : items.id,
                "rating" : items.rating,
                "review" : items.review,
                "created_at" : items.created_at,
                "updated_at" : items.updated_at
            }  
            result.append(review)
    
        return jsonify({"message":"Success","data":result})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def show(id):
    params = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(params)

    if dbresult.count() != 0:
        data = mysqldb.showReviewById(id,dbresult[0].id)
        
        if data.count() == 0:
            return jsonify({"message":"data not found"})

        review = {
            "id" : data[0].id,
            "rating" : data[0].rating,
            "review" : data[0].review,
            "created_at" : data[0].created_at,
            "updated_at" : data[0].updated_at
        }

        return jsonify({"message":"Success","data":review})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def insert(params):
    token = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(token)
    
    if dbresult.count() != 0:
        params['user_id'] = dbresult[0].id
        data = mysqldb.insertReview(params)
        return jsonify({"message" : "Success"})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def update(id,params):
    token = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(token)

    if dbresult.count() != 0:
        userid = dbresult[0].id
        data = mysqldb.updateReviewById(id,userid,params)
        return jsonify({"message" : "Success"})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def delete(id):
    params = get_jwt_identity()
    dbresult = mysqldb_user.showUserByEmail(params)

    if dbresult.count() != 0:
        data = mysqldb.deleteReviewById(id)
        return jsonify({"message" : "Success"})
    else:
        return jsonify({"message" : "Token Failed"})
from os import access
from app.models.user_model import Users as mysqldb
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime

@jwt_required()
def showAll():
    params = get_jwt_identity()
    dbresult = mysqldb.showUserByEmail(params)

    if dbresult.count() != 0:
        if dbresult[0].is_admin == 1:
            data = mysqldb.showUsers()
            result = []
            for items in data:
                user = {
                    "id" : items.id,
                    "email" : items.email,
                    "name" : items.name,
                    "created_at" : items.created_at,
                    "updated_at" : items.updated_at,
                }
                
                result.append(user)
            
            return jsonify({"message":"Success","data":result})
        else:
            return jsonify({"message" : "Unauthorized"})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def show():
    params = get_jwt_identity()
    dbresult = mysqldb.showUserByEmail(params)

    if dbresult.count() != 0:
        data = mysqldb.showUserById(dbresult[0].id)
        
        if data.count() == 0:
            return jsonify({"message":"data not found"})

        user = {
            "id" : data[0].id,
            "email" : data[0].email,
            "name" : data[0].name,
            "created_at" : data[0].created_at,
            "updated_at" : data[0].updated_at,
        }

        return jsonify({"message":"Success","data":user})
    else:
        return jsonify({"message" : "Token Failed"})

def insert(params):
    data = mysqldb.insertUser(params)
    return jsonify({"message" : "Success"})

@jwt_required()
def update(params):
    token = get_jwt_identity()
    dbresult = mysqldb.showUserByEmail(token)

    if dbresult.count() != 0:
        data = mysqldb.updateUserById(dbresult[0].id, params)
        return jsonify({"message" : "Success"})
    else:
        return jsonify({"message" : "Token Failed"})

@jwt_required()
def delete(id):
    params = get_jwt_identity()
    dbresult = mysqldb.showUserByEmail(params)

    if dbresult.count() != 0:
        if dbresult[0].is_admin == 1:
            data = mysqldb.deleteUserById(id)
            return jsonify({"message" : "Success"})
        else:
            return jsonify({"message" : "Unauthorized"})
    else:
        return jsonify({"message" : "Token Failed"})

def token(params):
    dbresult = mysqldb.showUserByEmail(params)
    
    if dbresult.count() != 0:
        #payload JWT
        user = {
            "email" : dbresult[0].email,
            "name"  : dbresult[0].name
        }

        expires = datetime.timedelta(days=1)
        access_token = create_access_token(user, fresh=True, expires_delta=expires)

        data = {
            "token_access" : access_token,
            "data" : user
        }
    else:
        data = {
            "message" : "Email not registered"
        }
    
    return jsonify(data)
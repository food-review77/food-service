from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:@localhost:3306/review_food', echo = True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print("Connection Success")

class Foods(Base):
    __tablename__ = 'foods'
    id = Column(Integer, primary_key =  True)
    name = Column(String)
    detail = Column(String)
    created_by = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def showFoods():
        try:
            query = session.query(Foods).all()
            return query
        except Exception as e:
            print(e)
        
    def showFoodById(userid):
        try:
            query = session.query(Foods).filter(Foods.id == userid)
            return query
        except Exception as e:
            print(e)

    def insertFood(params):
        try:
            date = datetime.now()
            params["created_at"] = date
            params["updated_at"] = date

            session.add(Foods(**params))
            session.commit()
        except Exception as e:
            print(e)

    def updateFoodById(userid,params):
        try:
            date = datetime.now()
            result = session.query(Foods).filter(Foods.id == userid).one()
            result.name = params['name']
            result.detail = params['detail']
            result.updated_at = date
            session.commit()

            result =  session.query(Foods).filter(Foods.id == userid).one()
            return result
            
        except Exception as e:
            print(e)

    def deleteFoodById(userid):
        try:
            result =  session.query(Foods).filter(Foods.id == userid).one()
            session.delete(result)
            session.commit()

        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
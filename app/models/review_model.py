from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:@localhost:3306/review_food', echo = True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print("Connection Success")

class Reviews(Base):
    __tablename__ = 'reviews'
    id = Column(Integer, primary_key =  True)
    user_id = Column(Integer)
    food_id = Column(Integer)
    review = Column(String)
    rating = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def showReviews(userid):
        try:
            query = session.query(Reviews).filter(Reviews.user_id == userid)
            return query
        except Exception as e:
            print(e)

    def showReviewById(reviewid,userid):
        try:
            query = session.query(Reviews).filter(Reviews.id == reviewid).filter(Reviews.user_id == userid)
            return query
        except Exception as e:
            print(e)

    def insertReview(params):
        try:
            date = datetime.now()
            params["created_at"] = date
            params["updated_at"] = date

            session.add(Reviews(**params))
            session.commit()
        except Exception as e:
            print(e)

    def updateReviewById(reviewid,userid,params):
        try:
            date = datetime.now()
            result = session.query(Reviews).filter(Reviews.id == reviewid).filter(Reviews.user_id == userid).one()
            result.food_id = params['food_id']
            result.rating = params['rating']
            result.review = params['review']
            result.updated_at = date
            session.commit()

            result =  session.query(Reviews).filter(Reviews.id == reviewid).filter(Reviews.user_id == userid).one()
            return result
            
        except Exception as e:
            print(e)

    def deleteReviewById(reviewid):
        try:
            result =  session.query(Reviews).filter(Reviews.id == reviewid).one()
            session.delete(result)
            session.commit()

        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
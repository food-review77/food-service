# from mysql.connector import connect, cursor
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from datetime import datetime

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:@localhost:3306/review_food', echo = True)
Session = sessionmaker(bind=engine)
session = Session()

if session:
    print("Connection Success")

class Users(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key =  True)
    email = Column(String)
    name = Column(String)
    is_admin = Column(Integer)
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    def showUsers():
        try:
            query = session.query(Users).all()
            return query
        except Exception as e:
            print(e)
        
    def showUserById(userid):
        try:
            query = session.query(Users).filter(Users.id == userid)
            return query
        except Exception as e:
            print(e)

    def showUserByEmail(params):
        try:
            query = session.query(Users).filter(Users.email == params["email"])
            return query
        except Exception as e:
            print(e)

    def insertUser(params):
        try:
            date = datetime.now()
            # params["is_admin"] = 0
            params["created_at"] = date
            params["updated_at"] = date

            session.add(Users(**params))
            session.commit()
        except Exception as e:
            print(e)

    def updateUserById(userid,params):
        try:
            date = datetime.now()
            result = session.query(Users).filter(Users.id == userid).one()
            result.email = params['email']
            result.name = params['name']
            result.updated_at = date
            session.commit()

            result =  session.query(Users).filter(Users.id == userid).one()
            return result
            
        except Exception as e:
            print(e)

    def deleteUserById(userid):
        try:
            result =  session.query(Users).filter(Users.id == userid).one()
            session.delete(result)
            session.commit()

        except Exception as e:
            print(e)
    
    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
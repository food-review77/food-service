from app import app
from app.controllers import food_controller
from flask import Blueprint, request

foods_blueprint = Blueprint("foods_router", __name__)

@app.route("/foods", methods=["GET"])
def showFoods():
    return food_controller.showAll()

@app.route("/food/<foodid>", methods=["GET"])
def showFood(foodid):
    return food_controller.show(foodid)

@app.route("/food/insert", methods=["POST"])
def insertFood():
    params = request.json
    return food_controller.insert(params)

@app.route("/food/update/<foodid>", methods=["POST"])
def updateFood(foodid):
    params = request.json
    return food_controller.update(foodid,params)

@app.route("/food/delete/<foodid>", methods=["DELETE"])
def deleteFood(foodid):
    return food_controller.delete(foodid)

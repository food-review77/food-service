from app import app
from app.controllers import review_controller
from flask import Blueprint, request

reviews_blueprint = Blueprint("reviews_router", __name__)

@app.route("/reviews", methods=["GET"])
def showReviews():
    return review_controller.showAll()

@app.route("/review/<reviewid>", methods=["GET"])
def showReview(reviewid):
    return review_controller.show(reviewid)

@app.route("/review/insert", methods=["POST"])
def insertReview():
    params = request.json
    return review_controller.insert(params)

@app.route("/review/update/<reviewid>", methods=["POST"])
def updateReview(reviewid):
    params = request.json
    return review_controller.update(reviewid,params)

@app.route("/review/delete/<reviewid>", methods=["DELETE"])
def deleteReview(reviewid):
    return review_controller.delete(reviewid)

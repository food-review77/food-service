from app import app
from app.controllers import user_controller
from flask import Blueprint, request

users_blueprint = Blueprint("users_router", __name__)

@app.route("/users", methods=["GET"])
def showUsers():
    return user_controller.showAll()

@app.route("/user", methods=["GET"])
def showUser():
    return user_controller.show()

@app.route("/user/insert", methods=["POST"])
def insertUser():
    params = request.json
    return user_controller.insert(params)

@app.route("/user/update", methods=["POST"])
def updateUser():
    params = request.json
    return user_controller.update(params)

@app.route("/user/delete/<iduser>", methods=["DELETE"])
def deleteUser(iduser):
    return user_controller.delete(iduser)

@app.route("/user/requesttoken", methods=["GET"])
def requestToken():
    params = request.json
    return user_controller.token(params)
CREATE TABLE `users` (
  `id` int PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) UNIQUE NOT NULL,
  `is_admin` boolean,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `foods` (
  `id` int PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `created_by` int,
  `created_at` datetime,
  `updated_at` datetime
);

CREATE TABLE `reviews` (
  `id` int PRIMARY KEY,
  `user_id` int,
  `food_id` int,
  `review` varchar(255),
  `rating` int NOT NULL,
  `created_at` datetime,
  `updated_at` datetime
);

ALTER TABLE `foods` ADD FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

ALTER TABLE `reviews` ADD FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

ALTER TABLE `reviews` ADD FOREIGN KEY (`food_id`) REFERENCES `foods` (`id`);
